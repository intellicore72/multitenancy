<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\Intellicore\Multitenancy\Tenant::class, function (Faker $faker) {
    $name = $faker->slug;

    return [
        'name' => $name,
        'slug' => $name
    ];
});
