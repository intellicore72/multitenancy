<?php

namespace Intellicore\Multitenancy;

use Illuminate\Database\Eloquent\Model;
use Intellicore\Multitenancy\TenantUser as User;

class Tenant extends Model
{
    protected $table = 'public.tenants';
    protected $fillable = ['name', 'slug'];
    protected $casts = [
        'settings' => 'array'
    ];

    public function Users()
    {
        return $this->belongsToMany(User::class, 'tenant_user', 'tenant_id', 'user_id');
    }

    public function HasUser($user)
    {
        return $this->users->contains($user);
    }

    public function getSidebarColorAttribute()
    {
        return $this->settings['sidebarColor'];
    }

    public function setSidebarColorAttribute($color)
    {
        $settings = $this->settings;

        $settings['sidebarColor'] = $color;

        $this->settings = $settings;

        $this->save();
    }
}
