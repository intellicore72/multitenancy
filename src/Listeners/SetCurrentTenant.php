<?php

namespace Intellicore\Multitenancy\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;
use Intellicore\Multitenancy\Tenant;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;

class SetCurrentTenant implements ManagesSchemas
{
    use ManagesSchemasTrait;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {

        if(session('current_tenant') == null) {
            $user = Auth::user();

            if($user->current_tenant == null)
            {
                $user->current_tenant = $user->tenancies->first()->slug;
                $user->save();
            }

            session(['current_tenant' => $user->current_tenant]);
        }

        if(session('sidebar_color') == null) {
            $tenant = Tenant::where('slug', session('current_tenant'))->first();

            session(['sidebar_color' => $tenant->sidebarColor]);

        }

        config(['bsa.sidebar' => session('sidebar_color')]);


        $this->switchTo(session('current_tenant'));

    }
}
