<?php


    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

// Internal API

    use Illuminate\Support\Facades\Auth;
    use Intellicore\Multitenancy\Tenant;

    Route::prefix('api/v1')->group(function () {
        Route::resource('auth/user', 'Intellicore\Multitenancy\TenantUserController');
    });

// Routes


    Route::post('/multitenancy/switch', 'Intellicore\Multitenancy\TenantController@switch')
        ->name('switch-tenant')
        ->middleware('web');


