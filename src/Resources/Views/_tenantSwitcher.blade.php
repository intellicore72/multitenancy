@if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->hasMultitenancies)

    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" method="post" action="/multitenancy/switch">
        {{ csrf_field() }}
        <div class="input-group">
            <select id="tenant-switcher" class="form-control bg-light border-0 small" name="tenant_id">
                @foreach(\Illuminate\Support\Facades\Auth::user()->tenancies as $tenant)
                <option value="{{ $tenant->id }}" @if($tenant->slug == session('current_tenant')) selected @endif> {{ $tenant->name }}</option>
                @endforeach
            </select>
            <div class="input-group-append">
                <button class="btn btn-primary" type="submit">
                    Switch Database
                </button>
            </div>
        </div>
    </form>

@endif
