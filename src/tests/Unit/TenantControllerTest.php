<?php

namespace Intellicore\Multitenancy\Tests\Unit;

use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;
use Intellicore\Multitenancy\Tenant;
use Intellicore\Multitenancy\TenantUser as User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TenantControllerTest extends TestCase implements ManagesSchemas
{
    use ManagesSchemasTrait;

    public function test_changes_the_current_tenant_session()
    {
        $this->drop('public');
        $this->drop('test_tenant');
        $this->drop('second_tenant');

        Artisan::call('setup:public');
        Artisan::call('setup:tenant "Test Tenant"');
        Artisan::call('setup:tenant "Second Tenant"');

        $user = factory(User::class)->create([]);
        $tenant1 = Tenant::find(1);
        $user->tenancies()->attach($tenant1);

        $tenant2 = Tenant::find(2);
        $user->tenancies()->attach($tenant2);

        Auth::login($user);
        session(['_previous' => ['url' => '/']]);

        $this->actingAs($user)
            ->post('/auth/switch-tenant', ['tenant_id' => 2])
            ->assertRedirect();

        $this->assertEquals(session('current_tenant'), 'second_tenant');

    }

    public function test_throws_an_error_if_user_doesnt_have_access_to_tenant()
    {
        $this->drop('public');
        $this->drop('test_tenant');
        $this->drop('second_tenant');

        Artisan::call('setup:public');
        Artisan::call('setup:tenant "Test Tenant"');
        Artisan::call('setup:tenant "Second Tenant"');

        $user = factory(User::class)->create([]);
        $tenant1 = Tenant::find(1);
        $user->tenancies()->attach($tenant1);

        Auth::login($user);
        session(['_previous' => ['url' => '/', 'error' => 'wrong error']]);
        $this->actingAs($user)
            ->post('/auth/switch-tenant', ['tenant_id' => 2])
            ->assertStatus(403);
    }

    public function test_throws_an_error_if_tenant_doesnt_exist()
    {
        $this->drop('public');
        $this->drop('test_tenant');

        Artisan::call('setup:public');
        Artisan::call('setup:tenant "Test Tenant"');

        $user = factory(User::class)->create([]);
        $tenant1 = Tenant::find(1);
        $user->tenancies()->attach($tenant1);

        $this->actingAs($user)
            ->post(route('switch-tenant'), ['tenant_id' => 2])
            ->assertStatus(404);
    }

    public function test_throws_an_error_if_schema_doesnt_exist()
    {

        $this->drop('public');
        $this->drop('test_tenant');

        Artisan::call('setup:public');
        Artisan::call('setup:tenant "Test Tenant"');

        $user = factory(User::class)->create([]);
        $tenant1 = Tenant::find(1);
        $user->tenancies()->attach($tenant1);

        $fakeTenant = Tenant::create([
            'name' => 'Not Real Tenant',
            'slug' => 'not_real_tenant'
        ]);

        Auth::login($user);
        session(['_previous' => ['url' => '/', 'error' => 'wrong error']]);
        $this->actingAs($user)
            ->post('/auth/switch-tenant', ['tenant_id' => $fakeTenant->id])
            ->assertStatus(410);
    }
}
