<?php

namespace Intellicore\Multitenancy\Tests\Unit;

use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;
use Intellicore\Multitenancy\Tenant;
use Intellicore\Multitenancy\TenantUser;
use Intellicore\Multitenancy\TenantUser as User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;
use Tests\TestCase;

class SetCurrentTenantTest extends TestCase implements ManagesSchemas
{
    use ManagesSchemasTrait;

    public function test_changes_the_current_tenant_session()
    {
       $this->drop('public');
       $this->drop('test_tenant');

       Artisan::call('setup:public');
       Artisan::call('setup:tenant "Test Tenant"');

       $tenant = Tenant::where('slug', 'test_tenant')->first();

       $tenant->sidebarColor = 'TestColor';

       $user = factory(TenantUser::class)->create([]);

       $tenant->users()->attach($user);

       $this->actingAs($user);

       $this->assertSame('TestColor', config('bsa.sidebar'));
    }


}
