<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;
use Tests\TestCase;

class ResetSchemasCommandTest extends TestCase implements ManagesSchemas
{
    use ManagesSchemasTrait;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_schema_exists_and_migrations_worked()
    {
        if($this->schemaExists('public')) {
            $this->drop('public');
        }

        if($this->schemaExists('first_tenant')) {
            $this->drop('first_tenant');
        }

        if($this->schemaExists('second_tenant')) {
            $this->drop('second_tenant');
        }


        Artisan::call('setup:public');
        Artisan::call('setup:tenant "First Tenant"');
        Artisan::call('setup:tenant "Second Tenant"');


        $this->assertTrue($this->schemaExists('public'));
        $this->assertTrue($this->schemaExists('first_tenant'));
        $this->assertTrue($this->schemaExists('second_tenant'));


        Artisan::call('setup:resetTestTenants');


        $this->assertTrue($this->schemaExists('public'));
        $this->assertTrue($this->schemaExists('first_tenant'));
        $this->assertTrue($this->schemaExists('second_tenant'));

        $this->assertDatabaseHas('tenants', ['slug' => 'second_tenant']);
    }
}
