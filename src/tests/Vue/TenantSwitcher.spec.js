import {mount} from 'vue-test-utils';
import expect from "expect";

import TenantSwitcher from "../../Resources/Assets/js/components/TenantSwitcher.vue";

describe('TenantSwitcher', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = mount(TenantSwitcher, {
                propsData: {
                    csrf: 'this',
                    currentTenant: {
                        name: "Current Tenant",
                        slug: "current_tenant"
                    },
                    tenants: [
                        {
                            name: "Other Tenant",
                            slug: "other_tenant",
                            id: 1,
                        },
                        {
                            name: "Current Tenant",
                            slug: "current_tenant",
                            id: 2,
                        }

                    ],
                }
        });
    });

    it('has a menu with the id of tenantSwitcher', () => {
        expect(wrapper.html()).toContain('name="tenant_id"');
    });

    it('contains the both tenants in the menu', () => {
        expect(wrapper.find('#tenant-switcher').html()).toContain('Current Tenant');
        expect(wrapper.find('#tenant-switcher').html()).toContain('Other Tenant');
    });
})
