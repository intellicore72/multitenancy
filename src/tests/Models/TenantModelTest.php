<?php

namespace Intellicore\Multitenancy\Tests\Unit;

use Illuminate\Support\Facades\Artisan;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;
use Intellicore\Multitenancy\Tenant;
use Intellicore\Multitenancy\TenantUser as User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TenantModelTest extends TestCase implements ManagesSchemas
{

    use ManagesSchemasTrait;

    public function test_tenant_can_be_saved()
    {

        $this->drop('public');
        Artisan::call('setup:public');

        $user = factory(\Intellicore\Multitenancy\TenantUser::class)->create([]);
        $tenant1 = Tenant::find(1);
        $user->tenancies()->attach($tenant1);

        $tenant = [
            'name' => 'New Tenant',
            'slug' => 'new_tenant'
        ];

        Tenant::create($tenant);

        $this->assertDatabaseHas('tenants', $tenant);
    }

    public function test_has_user_proves_a_user_has_access_to_tenant()
    {

        $this->drop('public');
        Artisan::call('setup:public');
        Artisan::call('setup:tenant "Test Tenant"');


        $tenant = Tenant::find(1);
        $user = factory(User::class)->create();

        $tenant->users()->attach($user);

        $this->assertTrue($tenant->hasUser($user));
    }

    public function test_has_user_proves_a_user_doesnt_have_access_to_tenant()
    {
        $this->drop('public');
        Artisan::call('setup:public');
        Artisan::call('setup:tenant "Test Tenant"');


        $tenant = Tenant::find(1);
        $user = factory(User::class)->create();

        $this->assertFalse($tenant->hasUser($user));
    }

    public function test_can_save_a_sidebar_color_setting()
    {
        $this->drop('public');
        $this->drop('test_tenant');

        Artisan::call('setup:public');
        Artisan::call('setup:tenant "Test Tenant"');

        $tenant = Tenant::where('slug', 'test_tenant')->first();

        $tenant->sidebarColor = '#4467A5';

        $this->assertSame('#4467A5', $tenant->sidebarColor);

    }
}
