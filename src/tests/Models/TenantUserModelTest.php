<?php

namespace Modules\IEPCore\Tests\Unit;

use Illuminate\Support\Facades\Artisan;
use Intellicore\Multitenancy\Tenant;
use Intellicore\Multitenancy\TenantUser;
use Intellicore\Multitenancy\TenantUser as User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TenantUserModelTest extends TestCase
{

    public function test_tenant_user_has_a_name_element()
    {
        $this->drop('public');
        Artisan::call('setup:public');

        $user = factory(TenantUser::class)->create();

        $this->assertSame($user->first_name . ' ' . $user->last_name, $user->name);
    }

}
