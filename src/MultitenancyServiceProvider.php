<?php

namespace Intellicore\Multitenancy;

use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\ServiceProvider;
use Intellicore\Multitenancy\Console\MigratePublicSchema;
use Intellicore\Multitenancy\Console\SetupResetTestTenantsCommand;
use Intellicore\Multitenancy\Console\SetupTenantDatabaseCommand;
use Intellicore\Multitenancy\Console\SetupPublicDatabaseCommand;
use Intellicore\Multitenancy\Listeners\SetCurrentTenant;

class MultitenancyServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->loadFactoriesFrom(__DIR__ . '/database/factories');

        $this->publishes([
            __DIR__.'/../config/multitenancy.php' => config_path('multitenancy.php'),
        ], 'multitenancy-config');

        $this->publishes([
            __DIR__.'/database/migrations' => $this->app->databasePath().'/migrations/public',
        ], 'multitenancy-migrations');

        $this->commands([
            MigratePublicSchema::class,
            SetupPublicDatabaseCommand::class,
            SetupTenantDatabaseCommand::class,
            SetupResetTestTenantsCommand::class

        ]);

        $this->loadViewsFrom(__DIR__.'/Resources/Views', 'multitenancy');


    }


    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/multitenancy.php', 'multitenancy');

        $this->app->make('Intellicore\Multitenancy\TenantController');
        $this->app->make('Intellicore\Multitenancy\TenantUserController');
        $this->app['events']->listen(Authenticated::class, SetCurrentTenant::class);

    }


}
