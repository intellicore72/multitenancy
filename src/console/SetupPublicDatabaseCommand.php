<?php

namespace Intellicore\Multitenancy\Console;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;

class SetupPublicDatabaseCommand extends Command implements ManagesSchemas
{
    use ManagesSchemasTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'setup:public';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('migrate:public');
    }
}
