<?php

namespace Intellicore\Multitenancy\Console;

use Illuminate\Console\Command;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;

class MigratePublicSchema extends Command implements ManagesSchemas
{
    use ManagesSchemasTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:public';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'uses migrations from migrations/public and adds them to the public schema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->create('public');

        $this->call('migrate', [
            '--path' => config('multitenancy.path_to_public_migrations')
        ]);
    }
}
