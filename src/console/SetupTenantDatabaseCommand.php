<?php

namespace Intellicore\Multitenancy\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;
use Intellicore\Multitenancy\Tenant;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;
use Symfony\Component\Console\Input\InputArgument;

class SetupTenantDatabaseCommand extends Command implements ManagesSchemas
{
    use ManagesSchemasTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'setup:tenant';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('tenantName');
        $slug = Str::snake($name);

        $this->create($slug);

        Tenant::create([
            'name' => $name,
            'slug' => $slug
        ]);

        $this->switchTo($slug);
        $this->migrateRefresh($slug);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['tenantName', InputArgument::REQUIRED, 'Tenant name.'],
        ];
    }
}
