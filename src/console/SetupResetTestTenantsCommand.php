<?php

namespace Intellicore\Multitenancy\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;

class SetupResetTestTenantsCommand extends Command implements ManagesSchemas
{
    use ManagesSchemasTrait;

    private $tenants;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:resetTestTenants';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset all Schemas ';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->schemaExists('public'))
            {$this->drop('public');}

        if($this->schemaExists('first_tenant'))
            {$this->drop('first_tenant');}

        if($this->schemaExists('second_tenant'))
            {$this->drop('second_tenant');}

        Artisan::call('setup:public');
        Artisan::call('setup:tenant "First Tenant"');
        Artisan::call('setup:tenant "Second Tenant"');
        Artisan::call('db:seed');

        $this->comment('All Schemas Reset.');
    }

}
