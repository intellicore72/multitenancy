<?php

namespace Intellicore\Multitenancy;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class TenantUser extends Authenticatable
{
    protected $table = 'public.users';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    #######################
    #
    #    Relationships
    #
    #######################

    /**
     * A collection of the Tenants the user is a member of.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tenancies()
    {
        return $this->belongsToMany(Tenant::class, 'tenant_user', 'user_id');
    }

    #######################
    #
    #    Attribute Getters
    #
    #######################


    /**
     * Does the user have access to more than one tenant.
     *
     * @return bool
     */
    public function getHasMultitenanciesAttribute()
    {
        return $this->tenancies()->count() > 1;
    }

    /**
     * The user's full name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim($this->first_name . ' ' . $this->last_name);
    }


    #######################
    #
    #    Query Scopes
    #
    #######################

    /**
     * @param $query
     * @param $tenantSlug
     * @return mixed
     */
    public function scopeInTenant($query, $tenantSlug)
    {
        return $query->whereHas('tenancies', static function ($query) use ($tenantSlug) {
            $query->where('slug', $tenantSlug);
        });
    }



}
