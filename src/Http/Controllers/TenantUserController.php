<?php

    namespace Intellicore\Multitenancy;

    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Routing\Controller;
    use Intellicore\Multitenancy\TenantUser as User;

    class TenantUserController extends Controller
    {
        /**
         * Display a listing of the resource.
         * @return Response
         */
        public function index()
        {
            return view('iepcore::index');
        }


        /**
         * Store a newly created resource in storage.
         * @param Request $request
         * @return Response
         */
        public function store(Request $request)
        {
            $user = $request->validate(
                [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|max:255'
                ]
            );

            $user['password'] = Carbon::now()->toString();

            return User::create($user);

        }

        /**
         * Show the specified resource.
         * @param int $id
         * @return Response
         */
        public function show($id)
        {
            return view('iepcore::show');
        }


        /**
         * Update the specified resource in storage.
         * @param Request $request
         * @param int $id
         * @return Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         * @param int $id
         * @return Response
         */
        public function destroy($id)
        {
            //
        }
    }
