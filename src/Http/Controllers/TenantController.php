<?php

namespace Intellicore\Multitenancy;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemasTrait;
use Intellicore\Multitenancy\ManagesSchemas\ManagesSchemas;

class TenantController extends Controller implements ManagesSchemas
{
    use ManagesSchemasTrait;

    public function switch(Request $request)
    {
        $user = Auth::user();

        $tenant = Tenant::find($request->get('tenant_id'));

        if($tenant == null) {
            abort(404, 'No database matches that ID');
        }

        elseif(! $this->schemaExists($tenant->slug)) {
            abort(410, 'That database does not exist');
        }

        elseif(! $tenant->hasUser($user)) {
            abort(403, 'You don\'t have access to that database.');
        }

        session(['current_tenant' => $tenant->slug]);
        session(['sidebar_color' => $tenant->sidebarColor]);

        $user->current_tenant = $tenant->slug;
        $user->save();


        return redirect(session('_previous')['url']);
    }
}
