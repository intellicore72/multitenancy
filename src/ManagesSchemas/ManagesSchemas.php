<?php

namespace Intellicore\Multitenancy\ManagesSchemas;

use Artisan;
use Config;
use DB;

interface ManagesSchemas
{
    /**
     * List all the available schemas
     *
     * @return mixed
     */
    public function listSchemas();

    /**
     * Check to see if a schema exists
     *
     * @param string $schemaName
     *
     * @return bool
     */
    public function schemaExists($schemaName);

    /**
     * Create a new schema
     *
     * @param string $schemaName
     */
    public function create($schemaName);

    /**
     * Set the search_path to the schema name
     *
     * @param string|array $schemaName
     */
    public function switchTo($schemaName = 'public');

    /**
     * Drop a schema
     *
     * @param string $schemaName
     */
    public function drop($schemaName);

    /**
     * Run migrations on a schema
     *
     * @param string $schemaName
     * @param array  $args
     */
    public function migrate($schemaName, $args = []);

    /**
     * Re-run all the migrations on a schema
     *
     * @param string $schemaName
     * @param array  $args
     */
    public function migrateRefresh($schemaName, $args = []);

    /**
     * Reverse all migrations on a schema
     *
     * @param string $schemaName
     * @param array  $args
     */
    public function migrateReset($schemaName, $args = []);

    /**
     * Rollback the latest migration on a schema
     *
     * @param string $schemaName
     * @param array  $args
     */
    public function migrateRollback($schemaName, $args = []);

    /**
     * Return the current search path
     *
     * @return string
     */
    public function getSearchPath();
}
