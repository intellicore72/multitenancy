### Intellicore \ Multitenancy

Basic package for providing multitenancy on the same domain through the use of PostgreSQL schemas.

#### Installation


1 Add the repo to `composer.json`

    "repositories": [
        ...
        {
            "type": "vcs",
            "url": "git@bitbucket.org:intellicore72/multitenancy.git"
        }
    ],
    
2 Require the package
```bash
composer require intellicore/multitenancy
```
    
3 For testing, you can seed the database by running `php artisan setup:resetTestTenants`.
This will generate the public schema and schemas First Tenant, and Second Tenant, 
then run a db:migrate. This will delete all data currently in the system and 
replace with test data.


#### Use Case
Client needs the same database to work for multiple legal entities. 
This package will allow users to be given access to one or many of the tenants.

#### Database Structure

The package requires a postgres database with admin permissions for the main user.
By running the `$ php artisan setup:public` command the public schema will be 
dropped and recreated with three tables, `users`, `tenants`, and `tenant_user`.
These tables are used for authenticating a user using Laravel's default authentication.

#### Setting the Schema

After a user has logged in, an event listener in this package will check if
the session variable `current_tenant` is set, if it is not, the logged in user's
attribute `current_tenant` will be set to the session variable. If the user
doesn't have a `current_tenant` then the `slug` of the first tenant the user
is associated with will be set as the current tenant. 

After the `current_tenant` session has been set, the listener will change the db search
path to `<current_tenant>, public`.

#### Package Elements

##### Templates

**multitenancies::_tenantSwitcher**
Provides a single line menu as a blade partial. Includes
a list of all tenants the signed in user has access to.
The can choose a new tenant and be switched to it.

**tenant-switcher**
Provides the same menu as a vue module. It must be passed as attributes
a `csrf` (string), `tenants` (json), and the `current_tenant` (json).

##### Models

**TenantUser**

The base laravel User model should be modified to extend
`TenantUser`.

**Tenant**

A record is created for each tenant that is created through the console command `setup:tenant`.

##### Console

**setup:public**

Used to set up the base schema. It will replace the current public schema and all related data.

**setup:tenant "[Tenant Name]"**

Used to set up a new tenant schema. You can pass a human readable string for the name of the
schema and it will be converted into a schema name.

##### Traits

**ManagesSchemas**

Provides a variety of functions for working with schemas

- schemaExists($schemaName)
    - Tests if a schema exists
- create($schemaName)
    - Creates a schema of that name
- switchTo($schemaName)
    - Adds the schema to the search path
- drop($schemaName)
    - Drops the named schema
- migrate($schemaName)
    - Runs the migrations in the schema
- migrateRefresh($schemaName)
    - Refreshes the migration under the schema name
- getSearchPath()
    - Returns the current search path as a string
