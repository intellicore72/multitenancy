<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    'ignore_schemas' => [
        'pg_toast',
        'pg_temp_1',
        'pg_toast_temp_1',
        'pg_catalog',
        'information_schema',
    ],
    'path_to_public_migrations' => 'database/migrations/public',
];
